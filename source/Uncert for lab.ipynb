{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is intended as an example to demonstrate the use of overloading in object oriented programming. This was written as a Jupyter notebook (aka IPython) in Python 3. To run in Python 2, simply rename the variables that have unicode names, and replace `truediv` with `div`.\n",
    "\n",
    "While there are several nice Python libraries that support uncertainty (for example, the powerful [uncertainties](https://pypi.python.org/pypi/uncertainties/) package and the related units and uncertainties package [pint](http://pint.readthedocs.org/en/0.6/)), they usually use standard error combination rules. For a beginning physics class, often 'maximum error' combination is used. Here, instead of using a standard deviation based error and using combination rules based on uncorrelated statistical distributions, we assume a simple maximum error and simply add errors.\n",
    "\n",
    "To implement this, let's build a Python class and use overloading to implement algebraic operations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import unittest\n",
    "import math"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The main rules are listed below.\n",
    "\n",
    "If $C=A+B$ or $C=A-B$, then\n",
    "$$\n",
    "\\delta C = \\delta A + \\delta B. \\tag{1}\n",
    "$$\n",
    "\n",
    "If $C=AB$ or $C=A/B$, then\n",
    "$$\n",
    "\\delta C = C_0 \\left( \\frac{\\delta A}{A_0} + \\frac{\\delta B}{B_0} \\right). \\tag{2}\n",
    "$$\n",
    "\n",
    "Following from that, if $C=A^n$ then\n",
    "$$\n",
    "\\delta C = A_0^n \\left( n \\frac{\\delta A}{A_0} \\right). \\tag{3}\n",
    "$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "class LabUnc(object):\n",
    "    @staticmethod\n",
    "    def combine(a, b):\n",
    "        return a + b\n",
    "\n",
    "    rounding_rule = 1\n",
    "    \"This is the number to round at for display, lab rule is 1, particle physics uses 3.54\"\n",
    "\n",
    "    def __init__(self, number, uncertainty=0):\n",
    "        self.n = number\n",
    "        self.s = abs(uncertainty)\n",
    "\n",
    "    @property\n",
    "    def ndigits(self):\n",
    "        v = math.ceil(-math.log10(self.s) + math.log10(self.rounding_rule))\n",
    "        return v if v > 0 else 0\n",
    "\n",
    "    @property\n",
    "    def max(self):\n",
    "        return self.n + self.s\n",
    "\n",
    "    @property\n",
    "    def min(self):\n",
    "        return self.n - self.s\n",
    "\n",
    "    def __repr__(self):\n",
    "        return \"{0}({1.n}, {1.s})\".format(type(self).__name__, self)\n",
    "\n",
    "    def __str__(self):\n",
    "        return (\n",
    "            format(self.n, \"0.\" + str(self.ndigits) + \"f\")\n",
    "            + \" ± \"\n",
    "            + format(self.s, \"0.\" + str(self.ndigits) + \"f\")\n",
    "        )\n",
    "\n",
    "    def _repr_html_(self):\n",
    "        return str(self)\n",
    "\n",
    "    def __eq__(self, other):\n",
    "        return abs(self.n - other.n) < 0.0000001 and abs(self.s - other.s) < 0.0000001\n",
    "\n",
    "    def __add__(self, other):  # (1)\n",
    "        return self.__class__(self.n + other.n, self.combine(self.s, other.s))\n",
    "\n",
    "    def __sub__(self, other):  # (1)\n",
    "        return self.__class__(self.n - other.n, self.combine(self.s, other.s))\n",
    "\n",
    "    def __mul__(self, other):  # (2)\n",
    "        C = self.n * other.n\n",
    "        δC = C * self.combine(self.s / self.n, other.s / other.n)\n",
    "        return type(self)(C, δC)\n",
    "\n",
    "    def __truediv__(self, other):  # (2)\n",
    "        C = self.n / other.n\n",
    "        δC = C * self.combine(self.s / self.n, other.s / other.n)\n",
    "        return self.__class__(C, δC)\n",
    "\n",
    "    def __pow__(self, power):  # (3)\n",
    "        C = self.n ** power\n",
    "        δC = C * (power * self.s / self.n)\n",
    "        return self.__class__(C, δC)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that we have a (hopefully!) working class, let's put together a quick unittest to see if it does what we expect."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "class TestLabUncCombine(unittest.TestCase):\n",
    "    def testAdd(self):\n",
    "        self.assertEqual(LabUnc(10, 1) + LabUnc(10, 2), LabUnc(20, 3))\n",
    "        self.assertEqual(LabUnc(20, 0.1) + LabUnc(10, 0.1), LabUnc(30, 0.2))\n",
    "\n",
    "    def testSub(self):\n",
    "        self.assertEqual(LabUnc(10, 1) - LabUnc(10, 2), LabUnc(0, 3))\n",
    "        self.assertEqual(LabUnc(20, 0.1) - LabUnc(10, 0.1), LabUnc(10, 0.2))\n",
    "\n",
    "    def testMul(self):\n",
    "        self.assertEqual(LabUnc(10, 1) * LabUnc(10, 2), LabUnc(100, 30))\n",
    "        self.assertEqual(LabUnc(20, 0.1) * LabUnc(10, 0.1), LabUnc(200, 3))\n",
    "\n",
    "    def testTrueDiv(self):\n",
    "        self.assertEqual(LabUnc(10, 1) / LabUnc(10, 2), LabUnc(1, 0.3))\n",
    "        self.assertEqual(LabUnc(20, 0.1) / LabUnc(10, 0.1), LabUnc(2, 0.03))\n",
    "\n",
    "    def testPow(self):\n",
    "        self.assertEqual(LabUnc(10, 1) ** 2, LabUnc(100, 20))\n",
    "        self.assertEqual(LabUnc(20, 0.1) ** 3, LabUnc(8000, 120))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Since we are putting our TestCase in a IPython notebook, we'll need to run it manually. This would be simpler if it was in a separate testing file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "testAdd (__main__.TestLabUncCombine) ... ok\n",
      "testMul (__main__.TestLabUncCombine) ... ok\n",
      "testPow (__main__.TestLabUncCombine) ... ok\n",
      "testSub (__main__.TestLabUncCombine) ... ok\n",
      "testTrueDiv (__main__.TestLabUncCombine) ... ok\n",
      "\n",
      "----------------------------------------------------------------------\n",
      "Ran 5 tests in 0.007s\n",
      "\n",
      "OK\n"
     ]
    }
   ],
   "source": [
    "suite = unittest.TestLoader().loadTestsFromTestCase(TestLabUncCombine)\n",
    "unittest.TextTestRunner(verbosity=2).run(suite);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can now say that our class works! Now, let's do some example calculations."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, we have a (in my opinion) much nicer syntax."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "A = LabUnc(3, 0.005)\n",
    "B = LabUnc(1.11, 0.05)\n",
    "C = LabUnc(0.004, 0.001)\n",
    "D = LabUnc(2.02, 0.08)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This notation is a little odd. Let's use the infix library to make the notation easier. We'll define `|pm|` to mean `+/-`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from infix import or_infix\n",
    "\n",
    "pm = or_infix(LabUnc)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "A = 3 | pm | 0.005\n",
    "B = 1.11 | pm | 0.05\n",
    "C = 0.004 | pm | 0.001\n",
    "D = 2.02 | pm | 0.08"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "A × B = 3.3 ± 0.2\n",
      "A × C = 0.012 ± 0.003\n",
      "A / B = 2.7 ± 0.1\n",
      "A / D = 1.49 ± 0.06\n",
      "A⁵ = 243 ± 2\n"
     ]
    }
   ],
   "source": [
    "print(\"A × B =\", A * B)\n",
    "print(\"A × C =\", A * C)\n",
    "print(\"A / B =\", A / B)\n",
    "print(\"A / D =\", A / D)\n",
    "print(\"A⁵ =\", A ** 5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "a = 12 | pm | 2\n",
    "b = 3 | pm | 0.5\n",
    "c = 2 | pm | 0.2\n",
    "d = (a / b) - c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "d₀ = 2.0\n",
      "d_max = 3.8\n",
      "d_min = 0.657142857142857\n",
      "δd = 1.5333333333333332\n"
     ]
    }
   ],
   "source": [
    "print(\"d₀ =\", d.n)\n",
    "print(\"d_max =\", a.max / b.min - c.min)\n",
    "print(\"d_min =\", a.min / b.max - c.max)\n",
    "print(\"δd =\", d.s)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "d₀ + δd = 3.533333333333333\n",
      "d₀ - δd = 0.4666666666666668\n"
     ]
    }
   ],
   "source": [
    "print(\"d₀ + δd =\", d.max)\n",
    "print(\"d₀ - δd =\", d.min)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Bonus: inheritance\n",
    "\n",
    "As a quick example of inheritance, let's set up the traditional uncertainty method using our class and inheritance. You may have noticed that the previous class was built with a combine method rather than simply adding uncertainties. Let's use the standard deviation rules to replace this combine with the one from traditional error analysis.\n",
    "\n",
    "If $C=A+B$ or $C=A-B$, then\n",
    "$$\n",
    "\\delta C = \\sqrt{\\delta A^2 + \\delta B^2}. \\tag{1}\n",
    "$$\n",
    "\n",
    "If $C=AB$ or $C=A/B$, then\n",
    "$$\n",
    "\\delta C = C_0 \\sqrt{ \\left( \\frac{\\delta A}{A_0}\\right)^2 + \\left(\\frac{\\delta B}{B_0}\\right)^2 }. \\tag{2}\n",
    "$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "class StdUnc(LabUnc):\n",
    "    @staticmethod\n",
    "    def combine(a, b):\n",
    "        return math.sqrt(a ** 2 + b ** 2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, we can test this; since there are libraries that handle uncertainty this way, we can compare to those. (I'll be using slightly more advanced testing methods here.) Notice that I used `n` and `s` for the value and uncertainty; this was to allow duck typing for the comparison method for the `uncertainties` library classes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from uncertainties import ufloat\n",
    "import itertools\n",
    "from operator import add, sub, mul, truediv"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "class TestStdUncCombine(unittest.TestCase):\n",
    "    def setUp(self):\n",
    "        cases = ((10, 1), (10, 2), (20, 0.1), (10, 0.1), (0.1234, 0.02))\n",
    "        self.pairs = itertools.permutations(cases, 2)\n",
    "\n",
    "    def run_operation_on_each(self, operation):\n",
    "        for a, b in self.pairs:\n",
    "            self.assertEqual(\n",
    "                operation(StdUnc(*a), StdUnc(*b)), operation(ufloat(*a), ufloat(*b))\n",
    "            )\n",
    "\n",
    "    def testAdd(self):\n",
    "        self.run_operation_on_each(add)\n",
    "\n",
    "    def testSub(self):\n",
    "        self.run_operation_on_each(sub)\n",
    "\n",
    "    def testMul(self):\n",
    "        self.run_operation_on_each(mul)\n",
    "\n",
    "    def testTrueDiv(self):\n",
    "        self.run_operation_on_each(truediv)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "testAdd (__main__.TestStdUncCombine) ... ok\n",
      "testMul (__main__.TestStdUncCombine) ... ok\n",
      "testSub (__main__.TestStdUncCombine) ... ok\n",
      "testTrueDiv (__main__.TestStdUncCombine) ... ok\n",
      "\n",
      "----------------------------------------------------------------------\n",
      "Ran 4 tests in 0.049s\n",
      "\n",
      "OK\n"
     ]
    }
   ],
   "source": [
    "suite = unittest.TestLoader().loadTestsFromTestCase(TestStdUncCombine)\n",
    "unittest.TextTestRunner(verbosity=2).run(suite);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#Extra Bonus:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's make a IPython extension that makes this even easier to use:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "from IPython.core.inputtransformer import StatelessInputTransformer"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 45,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from infix import custom_infix\n",
    "\n",
    "pm = custom_infix(\"__rmul__\", \"__mul__\")(ufloat)\n",
    "\n",
    "\n",
    "@StatelessInputTransformer.wrap\n",
    "def my_special_commands(line):\n",
    "    return line.replace(\"+/-\", \"*pm*\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 46,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import IPython\n",
    "\n",
    "ip = IPython.get_ipython()\n",
    "ip.input_splitter.logical_line_transforms.append(my_special_commands())\n",
    "ip.input_transformer_manager.logical_line_transforms.append(my_special_commands())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 54,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "-1209.0+/-2.8\n"
     ]
    }
   ],
   "source": [
    "print(112 +/- 2 - 1321 +/- 2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.4.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
