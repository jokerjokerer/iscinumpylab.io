---
title: "Setup an Apple Silicon Mac"
date: 2021-02-11T17:00:00-04:00
categories:
  - Apple
tags:
  - macos
---

I recently got an M1 mac, and I'll be cataloging my experience with using it
for scientific software development. I'll be returning to update this page
periodically, and will eventually have a focused recommendation for Apple
Silicon setup, similar to [my Intel setup](/post/setup-a-new-mac).

<!--more-->

## Base system observations

Before getting into setup, here are my observations on the base system setup.

* `python` is installed, 2.7.16, with the standard warning not to use it.
* `python3` (from the CLT) is 3.8.2 with pip 19.2.3.
* Note that Python 3.9 + Pip 21.0.1 are required for a proper experience on
  macOS, these must be custom Apple builds.
* `git` is 2.24.3, not bad at all.
* `ruby` is 2.6.3p62 and a Universal build, gives a warning about going away in
  the future. Good, built ins get in the way of brew.



## Basic setup

Always update macOS first thing. I forgot this, so had to reinstall the CLT
_and_ wipe and reinstall brew, since it was complaining that "git" was invalid.
Hopefully that won't happen again next update...

First things first, to install pretty much anything you need the "Xcode
command-line tools" (CLT). That is triggered automatically lots of different
ways, including typing `python3` into a terminal. The best way to trigger it
is:

```bash
xcode-select --install
```

This includes all the useful open-source tools Unix developers expect, like
git, clang, [and
more](http://osxdaily.com/2014/02/12/install-command-line-tools-mac-os-x/).
You'll have to agree to the license.


## Brew installs

Next, install [HomeBrew](https://brew.sh), this is the my favorite package
manager for Macs.

```bash
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

echo 'eval $(/opt/homebrew/bin/brew shellenv)' >> /Users/henryfs/.zprofile
eval $(/opt/homebrew/bin/brew shellenv)
```

This goes into `/opt/homebrew`, as is normal for the M1, instead of the normal
`/usr/local`. So far, `brew cask` has not been adapted, so you'll need to stick
to normal brewed bottles for now, and manually install apps like iTerm2 that
have been updated. Grr.

I'll have a new Brewfile eventually, but for now, carefully picking through my
old recommendations. Almost all of the "normal" installs I list in my Intel
post are bottled for Apple Silicon, I'm just being careful as I add things
back.

```bash
brew install numpy hugo gh macvim
```

You'll want to setup the `gh` tool with:

```bash
gh config set git_protocol ssh
```


#### SSH Key

If you want to download the brew bundle (or anything, really) from GitHub, it's
a good idea to [setup an SSH Key](/post/setting-up-ssh-forwarding/). In short:

```bash
ssh-keygen
# enter a passphrase
pbcopy < ~/.ssh/id_rsa.pub
```


This will keep you from entering the passphrase as long as you are logged in:


```bash
eval "$(ssh-agent -s)"
ssh-add ~/.ssh/id_rsa
```

And add the following to `.ssh/config`:

```text
Host *
  AddKeysToAgent yes
  UseKeychain yes
  IdentityFile ~/.ssh/id_rsa
```

While you are at it, feel free to set `git config --global user.name` and `git
config --global user.email`.

## Quality of life

I like to change capslock into escape, which is in keyboard settings, and must
be done per-keyboard. Also, the keyboard repeat was very slow for some reason.
