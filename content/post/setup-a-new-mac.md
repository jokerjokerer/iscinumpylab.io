---
title: "Setup a New Mac"
date: 2019-07-18T11:01:06-04:00
lastmod: 2021-02-11T17:00:00-04:00
categories:
  - Apple
tags:
  - macos
---

Here is a list of my favorite things to do to properly setup macOS for scientific work. I periodically update it; feel free to leave a comment if something breaks. This should work on macOS 11 on Intel; see [my post on Apple Silicon](/post/setup-apple-silicon) to track progress on a similar setup.


<!--more-->

## Basic setup

First things first, to install pretty much anything you need Xcode. Go grab it from the Mac AppStore. You'll need the "command-line tools" as well, but that will be installed automatically by the next step. If you want to do it manually, type this in a terminal:

```bash
xcode-select --install
sudo xcodebuild -license
```

This includes all the useful open-source tools Unix developers expect, like git, clang, [and more](http://osxdaily.com/2014/02/12/install-command-line-tools-mac-os-x/). You'll have to agree to the license.

{{< details "If this ever breaks (click to expand) " >}}
The following lines will fix problems if this breaks:

```bash
xcode-select --reset
sudo xcodebuild -license
```
{{< / details >}}

Next, install [HomeBrew](https://brew.sh), this is the my favorite package manager for Macs.

```bash
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```

## Brew installs

I am providing a brew bundle file [here](https://gitlab.com/iscinumpy/iscinumpy.gitlab.io/blob/master/Brewfile). It can be installed by running `brew bundle`. See the [docs here](https://github.com/Homebrew/homebrew-bundle).
Or see a nice [blog post here](https://thoughtbot.com/blog/brewfile-a-gemfile-but-for-homebrew) for a quick introduction to bundles.

{{< code file="/Brewfile" language="bash" >}}

If you want to install these by hand, just change `brew "xxx"` to `brew install xxx` on the command line, and `cask "xxx"` to `brew install cask xxx`.
You shouldn't need to "tap" the casks, though if you want the fonts I'm using, you will want to either tap it or run .

Anything else that is not open-source, or is hard to build, is still installable by homebrew as a cask. Plus, you can upgrade casks, so this is slightly better than installing the package by hand. (Casks are homebrew formula that just wrap an existing installer).

## Other

#### ITerm2

You should open up ITerm2 (installed above) and make it the default shell and install the terminal utilities (both in the main menu). Pin to dock. Set SauceCodePro Nerd Font as the main font (you can/should do this in the normal terminal, too).

#### SSH Key

If you want to download the brew bundle (or anything, really) from GitHub, it's a good idea to [setup an SSH Key](/post/setting-up-ssh-forwarding/). In short:

```bash
ssh-keygen
# enter a passphrase
pbcopy < ~/.ssh/id_rsa.pub
eval "$(ssh-agent -s)"
ssh-add ~/.ssh/id_rsa
```

And add the following to `.ssh/config`:

```text
Host *
  AddKeysToAgent yes
  UseKeychain yes
  IdentityFile ~/.ssh/id_rsa
```

While you are at it, feel free to set `git config --global user.name` and `git config --global user.email`.

### Fish

If you want to use fish, add `/usr/local/bin/fish` to `/etc/shells` and then run:

```bash
chsh -s /usr/local/bin/fish
```

Install [fisher](https://github.com/jorgebucaran/fisher):

```bash
curl https://git.io/fisher --create-dirs -sLo ~/.config/fish/functions/fisher.fish
```

This works best when you use the extended fonts in your terminal (Select SauceCodePro Nerd Font), and then run:

```bash
set -g theme_nerd_fonts yes
fisher add oh-my-fish/theme-bobthefish
```

> #### Note:
>
> Now that zsh comes default, there is less of a need to switch. You may prefer [Oh MyZsh](https://ohmyz.sh) instead. Use `sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"` to install.

Here are a few useful environment variables to set. I don't like getting too far from the default, but these are pretty useful. These persist across logins:

```fish
set -Ux CMAKE_GENERATOR Ninja
set -Ux VIRTUAL_ENV_DISABLE_PROMPT 1
set -Ux CTEST_OUTPUT_ON_FAILURE 1
```

Use `set -UxL` to list.


### Jupyterlab and key Python utilities

Since you can run kernels from Conda in any JupyterLab install, I like to install it directly into the Homebrew Python 3 instance - but this is now handled through a brew formula for jupyterlab. So far, I still sometimes install the following utilities. You probably should *not* do this, and just allow homebrew to manage your Python instance, using virtual environment for *everything* else. But if you do, then run this:

```bash
python3 -m pip install pyforest plumbum
```

Now, make sure you install `nb_conda_kernels` into your conda environments, and you'll be able to see them without activating the environments!

### Quality of life

I like to change capslock into escape, which is in keyboard settings, and must be done per-keyboard.

### VIM

It is important to have installed macvim from brew directly, and not the cask, or otherwise the vi command will not be changed to the new vim. Here is my vimrc, which includes directions. I used to use the ultimate VIM package, but this is much more flexible; still adjusting to fill in gaps.
Things will break if you use the system vim; you should at least run:

```bash
git config --global core.editor $(which vim)
```

to make sure you do not open the old vim instead when committing with git. Remove the `$` if you took my suggestion and are using fish.

Remember to run the ycmd setup script after starting up vim!

{{< code file="/vimrc" language="vim" >}}
