---
title: University of Texas Doctoral Thesis Template
date: 2015-07-12T17:05:00.000-07:00
lastmod: 2015-07-18T19:44:11.681-07:00
categories:
  - UTexas
tags:
  - latex
---

I have created a thesis class file for a UT Thesis in LaTeX. It has already been used for at least one passing thesis, so it does meet the current UT guidelines. (Please let me know if there are any issues!)

Since I use Bitbucket for all my private repositories (like my thesis itself), the code is in a Bitbucket repository rather than GitHub. Here is the link if you want to create a pull request or want to compile the class and documentation from the source .dtx file.

If all you want is a download of a working version, and if you don't want to compile the code but just want the class file, [here are downloadable packages including the class file](https://bitbucket.org/henryiii/texutthesis/downloads).

## Original email

This was originally released in the following email to the UT Gradlist:

Due to the pitiful lack of vaguely resent or usable resources for a UT thesis in LaTeX, I have created a thesis template in LaTeX to match the UT guidelines. The repository for the source can be found on BitBucket here, and a downloadable package is here, as texutthesis.zip. Please let me know if you have improvements.

To use the class in the download, put the utthesis.cls file in your folder with your thesis (recommended), or put it in a LaTeX searchable location. If you have the source, run "pdflatex utthesis.dtx" to generate the .cls file (and a helpful pdf guide). I've included a sample thesis too.

Most of the time, this works as expected, modifying and theming the standard commands. It does add a few custom commands, like UTabstract, when something special is needed. I followed the UT guideline as closely as possible. Internally, it uses Memoir to provide precise formatting control.

If you don't have time to look through the sample, here is an abbreviated sample to whet your appetite:

```tex
\documentclass{utthesis}

\UTyear=2015

\begin{document}

\author{John Fredrick Doe}
\title{Very Important Title For a Very Important Thesis}
\date{Revised: \today}

\UTcopyrightlegend

\begin{UTcommittee}
\UTaddsupervisor{George Washington}
\UTaddcosupervisor{Chuck Norris}
\UTaddcommittee{John Adams}
\UTaddcommittee{Thomas Jefferson}
\end{UTcommittee}

\UTtitlepage{B.S.}{May}

\frontmatter

\begin{UTabstract}[s]{George Washington, Chuck Norris}
I did some cool research.
\end{UTabstract}

...

\end{document}
```

Hopefully some of you will find this useful.
