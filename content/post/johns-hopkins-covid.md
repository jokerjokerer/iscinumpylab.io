---
title: "Johns Hopkins COVID-19 Dataset in Pandas"
date: 2020-03-30T15:30:00-04:00
lastMod: 2020-04-01T21:20:00-04:00
categories:
  - Python
tags:
  - programming
  - python
  - pandas
  - pandemic
---
COVID-19 is ravaging the globe. Let's look at the excellent Johns Hopkins
dataset using Pandas. This will serve both as a guideline for getting the data
and exploring on your own, as well as an example of Pandas multi-indexing in an easy
to understand situation. I am currently involved in [science-responds](https://science-responds.org).

<!--more-->

<!--
Note: "manually" (S&R) replace all divs with tables with {{< pandas >}} ... {{< /pandas >}}
      replace python with python3

In your ipython profile (ipython profile create, then in ~/.ipython/profile_default/ipython_kernel_config.py add:
c.InlineBackend.figure_format = 'retina'
-->

```python3
# Python 3.6+ required
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from urllib.error import HTTPError
plt.style.use('ggplot')
```

My favorite links:
[worldometer](https://www.worldometers.info/coronavirus/) &bullet;
[arcGIS](https://www.arcgis.com/apps/opsdashboard/index.html#/bda7594740fd40299423467b48e9ecf6) &bullet;
[Projections](https://covid19.healthdata.org)

A few more:
[COVID-19 dash](https://covid19dashboards.com) &bullet;
[nCoV2019](https://ncov2019.live) &bullet;
[91-DIVOC](http://91-divoc.com/pages/covid-visualization)

Anyway, now that we've made some basic imports, let's write a function that can read in a datafile from GitHub:


```python3
def get_day(day: pd.Timestamp):

    # Read in a datafile from GitHub
    try:
        table = pd.read_csv(
            "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/"
            "master/csse_covid_19_data/csse_covid_19_daily_reports/"
            f"{day:%m-%d-%Y}.csv",
        )
    except HTTPError:
        return pd.DataFrame()

    # Cleanup - sadly, the format has changed a bit over time - we can normalize that here
    table.columns = [
        f.replace("/", "_")
        .replace(" ", "_")
        .replace("Latitude", "Lat")
        .replace("Longitude", "Long_")
        for f in table.columns
    ]

    # This column is new in recent datasets
    if "Admin2" not in table.columns:
        table["Admin2"] = None

    # New datasets have these, but they are not very useful for now
    table.drop(
        columns=["FIPS", "Combined_Key", "Lat", "Long_"], errors="ignore", inplace=True
    )

    # If the last update time was useful, we would make this day only, rather than day + time
    #   table["Last_Update"] = pd.to_datetime(table["Last_Update"]).dt.normalize()
    #
    # However, last update is odd, let's just make this the current day
    table["Last_Update"] = day

    # Make sure indexes are not NaN, which causes later bits to not work. 0 isn't
    # perfect, but good enough.
    # Return as a multindex
    return table.fillna(0).set_index(
        ["Last_Update", "Country_Region", "Province_State", "Admin2"], drop=True
    )
```

Now let's loop over all days and build a multi-index DataFrame with the whole
dataset. We'll be doing quite a bit of cleanup here as well. If you do this
outside of a function, you should never modify an object in multiple cells;
ideally you create an object like `df`, and make any modifications and
replacements in the same cell. That way, running any cell again or running a
cell multiple times will not cause unusual errors and problems to show up.



```python3
def get_all_days(end_day = None):

    # Assume current day - 1 is the latest dataset if no end given
    if end_day is None:
        end_day = pd.Timestamp.now().normalize()

    # Make a list of all dates
    date_range = pd.date_range("2020-01-22", end_day)

    # Create a generator that returns each day's dataframe
    day_gen = (get_day(day) for day in date_range)

    # Make a big dataframe, NaN is 0
    df = pd.concat(day_gen).fillna(0).astype(int)

    # Remove a few duplicate keys
    df = df.groupby(level=df.index.names).sum()

    # Sometimes active is not filled in; we can compute easily
    df["Active"] = np.clip(
        df["Confirmed"] - df["Deaths"] - df["Recovered"], 0, None
    )

    # Change in confirmed cases (placed in a pleasing location in the table)
    df.insert(
        1,
        "ΔConfirmed",
        df.groupby(level=("Country_Region", "Province_State", "Admin2"))["Confirmed"]
        .diff()
        .fillna(0)
        .astype(int),
    )

    # Change in deaths
    df.insert(
        3,
        "ΔDeaths",
        df.groupby(level=("Country_Region", "Province_State", "Admin2"))["Deaths"]
        .diff()
        .fillna(0)
        .astype(int),
    )

    return df
```

If this were a larger/real project, it would be time to bundle up the functions
above and put them into a `.py` file - notebooks are for experimentation,
teaching, and high level manipulation. Functions and classes should normally
move to normal Python files when ready.

Let's look at a few lines of this DataFrame to see what we have:


```python3
df = get_all_days()
df
```




{{< pandas >}}
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th>Confirmed</th>
      <th>ΔConfirmed</th>
      <th>Deaths</th>
      <th>ΔDeaths</th>
      <th>Recovered</th>
      <th>Active</th>
    </tr>
    <tr>
      <th>Last_Update</th>
      <th>Country_Region</th>
      <th>Province_State</th>
      <th>Admin2</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th rowspan="5" valign="top">2020-01-22</th>
      <th>Hong Kong</th>
      <th>Hong Kong</th>
      <th>0</th>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>Japan</th>
      <th>0</th>
      <th>0</th>
      <td>2</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>2</td>
    </tr>
    <tr>
      <th>Macau</th>
      <th>Macau</th>
      <th>0</th>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th rowspan="2" valign="top">Mainland China</th>
      <th>Anhui</th>
      <th>0</th>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>Beijing</th>
      <th>0</th>
      <td>14</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>14</td>
    </tr>
    <tr>
      <th>...</th>
      <th>...</th>
      <th>...</th>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th rowspan="5" valign="top">2020-04-01</th>
      <th>Venezuela</th>
      <th>0</th>
      <th>0</th>
      <td>143</td>
      <td>8</td>
      <td>3</td>
      <td>0</td>
      <td>41</td>
      <td>99</td>
    </tr>
    <tr>
      <th>Vietnam</th>
      <th>0</th>
      <th>0</th>
      <td>218</td>
      <td>6</td>
      <td>0</td>
      <td>0</td>
      <td>63</td>
      <td>155</td>
    </tr>
    <tr>
      <th>West Bank and Gaza</th>
      <th>0</th>
      <th>0</th>
      <td>134</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
      <td>18</td>
      <td>115</td>
    </tr>
    <tr>
      <th>Zambia</th>
      <th>0</th>
      <th>0</th>
      <td>36</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>36</td>
    </tr>
    <tr>
      <th>Zimbabwe</th>
      <th>0</th>
      <th>0</th>
      <td>8</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>7</td>
    </tr>
  </tbody>
</table>
<p>41128 rows × 6 columns</p>
{{< /pandas >}}



The benefit of doing this all at once, in one DataFrame, should quickly become
apparent. We can now use simple selection and grouping to "ask" almost anything
about our dataset.

As an example, let's look at just the US portion of the dataset. We'll use the
pandas selection `.xs`:



```python3
us = df.xs("US", level="Country_Region")
us
```




{{< pandas >}}
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th></th>
      <th></th>
      <th>Confirmed</th>
      <th>ΔConfirmed</th>
      <th>Deaths</th>
      <th>ΔDeaths</th>
      <th>Recovered</th>
      <th>Active</th>
    </tr>
    <tr>
      <th>Last_Update</th>
      <th>Province_State</th>
      <th>Admin2</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2020-01-22</th>
      <th>Washington</th>
      <th>0</th>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>2020-01-23</th>
      <th>Washington</th>
      <th>0</th>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th rowspan="2" valign="top">2020-01-24</th>
      <th>Chicago</th>
      <th>0</th>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>Washington</th>
      <th>0</th>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>2020-01-25</th>
      <th>Illinois</th>
      <th>0</th>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>...</th>
      <th>...</th>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th rowspan="4" valign="top">2020-03-31</th>
      <th rowspan="4" valign="top">Wyoming</th>
      <th>Sublette</th>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>Sweetwater</th>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>2</td>
    </tr>
    <tr>
      <th>Teton</th>
      <td>20</td>
      <td>4</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>20</td>
    </tr>
    <tr>
      <th>Washakie</th>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>2020-04-01</th>
      <th>Recovered</th>
      <th>0</th>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>8536</td>
      <td>0</td>
    </tr>
  </tbody>
</table>
<p>32352 rows × 6 columns</p>
{{< /pandas >}}



Notice we have counties (early datasets just have one "county" called `"0"`).
If we were only interested in states, we can group by the remaining levels and
sum out the `"Admin2"` (county and similar) dimension:


```python3
by_state = us.groupby(level=("Last_Update", "Province_State")).sum()
by_state
```




{{< pandas >}}
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th></th>
      <th>Confirmed</th>
      <th>ΔConfirmed</th>
      <th>Deaths</th>
      <th>ΔDeaths</th>
      <th>Recovered</th>
      <th>Active</th>
    </tr>
    <tr>
      <th>Last_Update</th>
      <th>Province_State</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2020-01-22</th>
      <th>Washington</th>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>2020-01-23</th>
      <th>Washington</th>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th rowspan="2" valign="top">2020-01-24</th>
      <th>Chicago</th>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>Washington</th>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>2020-01-25</th>
      <th>Illinois</th>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>...</th>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th rowspan="4" valign="top">2020-03-31</th>
      <th>Washington</th>
      <td>5432</td>
      <td>509</td>
      <td>225</td>
      <td>20</td>
      <td>0</td>
      <td>5207</td>
    </tr>
    <tr>
      <th>West Virginia</th>
      <td>162</td>
      <td>17</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>161</td>
    </tr>
    <tr>
      <th>Wisconsin</th>
      <td>1412</td>
      <td>182</td>
      <td>25</td>
      <td>5</td>
      <td>0</td>
      <td>1387</td>
    </tr>
    <tr>
      <th>Wyoming</th>
      <td>109</td>
      <td>15</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>109</td>
    </tr>
    <tr>
      <th>2020-04-01</th>
      <th>Recovered</th>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>8536</td>
      <td>0</td>
    </tr>
  </tbody>
</table>
<p>2151 rows × 6 columns</p>
{{< /pandas >}}



Using the same selector as before, we can pick out North Carolina:


```python3
by_state.xs("North Carolina", level="Province_State")
```




{{< pandas >}}
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Confirmed</th>
      <th>ΔConfirmed</th>
      <th>Deaths</th>
      <th>ΔDeaths</th>
      <th>Recovered</th>
      <th>Active</th>
    </tr>
    <tr>
      <th>Last_Update</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2020-03-10</th>
      <td>7</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>7</td>
    </tr>
    <tr>
      <th>2020-03-11</th>
      <td>7</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>7</td>
    </tr>
    <tr>
      <th>2020-03-12</th>
      <td>15</td>
      <td>8</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>15</td>
    </tr>
    <tr>
      <th>2020-03-13</th>
      <td>17</td>
      <td>2</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>17</td>
    </tr>
    <tr>
      <th>2020-03-14</th>
      <td>24</td>
      <td>7</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>24</td>
    </tr>
    <tr>
      <th>2020-03-15</th>
      <td>33</td>
      <td>9</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>33</td>
    </tr>
    <tr>
      <th>2020-03-16</th>
      <td>38</td>
      <td>5</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>38</td>
    </tr>
    <tr>
      <th>2020-03-17</th>
      <td>64</td>
      <td>26</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>64</td>
    </tr>
    <tr>
      <th>2020-03-18</th>
      <td>70</td>
      <td>6</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>70</td>
    </tr>
    <tr>
      <th>2020-03-19</th>
      <td>123</td>
      <td>53</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>123</td>
    </tr>
    <tr>
      <th>2020-03-20</th>
      <td>172</td>
      <td>49</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>172</td>
    </tr>
    <tr>
      <th>2020-03-21</th>
      <td>253</td>
      <td>81</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>253</td>
    </tr>
    <tr>
      <th>2020-03-22</th>
      <td>305</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>305</td>
    </tr>
    <tr>
      <th>2020-03-23</th>
      <td>353</td>
      <td>48</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>353</td>
    </tr>
    <tr>
      <th>2020-03-24</th>
      <td>495</td>
      <td>142</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>495</td>
    </tr>
    <tr>
      <th>2020-03-25</th>
      <td>590</td>
      <td>95</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>589</td>
    </tr>
    <tr>
      <th>2020-03-26</th>
      <td>738</td>
      <td>148</td>
      <td>3</td>
      <td>2</td>
      <td>0</td>
      <td>735</td>
    </tr>
    <tr>
      <th>2020-03-27</th>
      <td>887</td>
      <td>149</td>
      <td>4</td>
      <td>1</td>
      <td>0</td>
      <td>883</td>
    </tr>
    <tr>
      <th>2020-03-28</th>
      <td>1020</td>
      <td>133</td>
      <td>5</td>
      <td>1</td>
      <td>0</td>
      <td>1015</td>
    </tr>
    <tr>
      <th>2020-03-29</th>
      <td>1191</td>
      <td>171</td>
      <td>7</td>
      <td>2</td>
      <td>0</td>
      <td>1184</td>
    </tr>
    <tr>
      <th>2020-03-30</th>
      <td>1313</td>
      <td>122</td>
      <td>7</td>
      <td>-1</td>
      <td>0</td>
      <td>1307</td>
    </tr>
    <tr>
      <th>2020-03-31</th>
      <td>1535</td>
      <td>222</td>
      <td>12</td>
      <td>6</td>
      <td>0</td>
      <td>1523</td>
    </tr>
  </tbody>
</table>
{{< /pandas >}}



We can look at all of US, as well:


```python3
all_states = by_state.groupby(level="Last_Update").sum()
all_states
```




{{< pandas >}}
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Confirmed</th>
      <th>ΔConfirmed</th>
      <th>Deaths</th>
      <th>ΔDeaths</th>
      <th>Recovered</th>
      <th>Active</th>
    </tr>
    <tr>
      <th>Last_Update</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2020-01-22</th>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>2020-01-23</th>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>2020-01-24</th>
      <td>2</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>2</td>
    </tr>
    <tr>
      <th>2020-01-25</th>
      <td>2</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>2</td>
    </tr>
    <tr>
      <th>2020-01-26</th>
      <td>5</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>5</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>2020-03-28</th>
      <td>121478</td>
      <td>19124</td>
      <td>2026</td>
      <td>448</td>
      <td>1072</td>
      <td>119576</td>
    </tr>
    <tr>
      <th>2020-03-29</th>
      <td>140886</td>
      <td>19394</td>
      <td>2467</td>
      <td>434</td>
      <td>2665</td>
      <td>138598</td>
    </tr>
    <tr>
      <th>2020-03-30</th>
      <td>161807</td>
      <td>20652</td>
      <td>2978</td>
      <td>512</td>
      <td>5644</td>
      <td>159145</td>
    </tr>
    <tr>
      <th>2020-03-31</th>
      <td>188172</td>
      <td>26452</td>
      <td>3873</td>
      <td>886</td>
      <td>7024</td>
      <td>184808</td>
    </tr>
    <tr>
      <th>2020-04-01</th>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>8536</td>
      <td>0</td>
    </tr>
  </tbody>
</table>
<p>71 rows × 6 columns</p>
{{< /pandas >}}



#### US total cases

Let's try a simple plot first; this is the one you see quite often.


```python3
plt.figure(figsize=(10,5))
all_states.Confirmed.plot(logy=True, style='o');
```


![png](output_19_0.png)


#### Italy, new cases per day

As another example, let's view the new cases per day for Italy. We will add a rolling mean, just to help guide the eye through the fluctuations - it is not a fit or anything fancy.


```python3
interesting = df.xs("Italy", level="Country_Region").groupby(level="Last_Update").sum()

plt.figure(figsize=(10,5))
interesting.ΔConfirmed.rolling(5, center=True).mean().plot(style='-', label='Rolling mean')
interesting.ΔConfirmed.plot(style='o', label="Data")
plt.ylabel("New cases per day")
plt.legend();
```


![png](output_21_0.png)


#### Italy, transmission rate

It's more interesting to instead look at the transmission rate per day, which is new cases / active cases. The colors in the plot start changing when Italy implemented a lockdown on the 11th, and change over 14 days, which is roughly 1x the time to first symptoms. The lockdown make take longer than that to take full effect. There were several partial steps taken before the full lockdown on the 4th and 9th. Notice the transmission is slowing noticeably!


```python3
interesting = df.xs("Italy", level="Country_Region").groupby(level="Last_Update").sum()
growth = interesting.ΔConfirmed / interesting.Active
growth = growth['2020-02-24':]

# Color based on lockdown (which happened in 3 stages, 4th, 9th, and 11th)
lockdown = growth.index - pd.Timestamp('2020-03-11')
lockdown = np.clip(lockdown.days, 0, 14) / 14

fix, ax = plt.subplots(figsize=(10,5))
ax.scatter(growth.index, growth, cmap='cool', c=lockdown)

ax.set_ylabel("new cases / active cases")

#set ticks every week
ax.xaxis.set_major_locator(mdates.WeekdayLocator())
#set major ticks format
ax.xaxis.set_major_formatter(mdates.DateFormatter('%b %d'))

```


![png](output_23_0.png)


#### US, transmission rate

Same plot for the US. The colors in the plot start changing when the US started the 15 plan to slow the spread, and change over 14 days, which is roughly 1x the time to first symptoms. Each state has implemented different guidelines, so the effect will be spread out even further. Again, we are see the effect of the lockdown!


```python3
interesting = df.xs("US", level="Country_Region").groupby(level="Last_Update").sum()
growth = interesting.ΔConfirmed / interesting.Active
growth = growth['2020-03-01':]

# Not really a full lockdown, just a distancing guideline + local lockdowns later
lockdown = growth.index - pd.Timestamp('2020-03-15')
lockdown = np.clip(lockdown.days, 0, 14) / 14

fix, ax = plt.subplots(figsize=(10,5))
ax.scatter(growth.index, growth, cmap='cool', c=lockdown)

ax.set_ylabel("new cases / active cases")

#set ticks every week
ax.xaxis.set_major_locator(mdates.WeekdayLocator())
#set major ticks format
ax.xaxis.set_major_formatter(mdates.DateFormatter('%b %d'))
```


![png](output_25_0.png)
