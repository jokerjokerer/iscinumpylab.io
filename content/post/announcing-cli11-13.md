---
title: "Announcing CLI11 1.3"
date: 2017-12-01T09:03:50-05:00
categories:
  - cpp
tags:
  - programming
  - cpp
  - cli
  - cli11
  - plumbum
---

[CLI11], a powerful library for writing beautiful command line interfaces in C++11, has [been updated to 1.3][releases], the largest update ever. CLI11 is more powerful than ever, and has simpler and more consistent parsing under the hood.

This version focused on refactoring several key systems to ensure correct behavior in the interaction of different settings. Most caveats about features only working on the main App have been addressed, and extra arguments have been reworked. Inheritance of defaults makes configuring CLI11 much easier without having to subclass. Policies add new ways to handle multiple arguments to match your favorite CLI programs. Error messages and help messages are better and more flexible. Several bugs and odd behaviors in the parser have been fixed.


<!--more-->

## Key new features:

### Inheritance

Most App settings are inherited by subcommands, and a new default option template lets you set values once for all. For example, if you want all options to be case insensitive:

```cpp
app.option_defaults()->case_insensitive();
```

Now if you create an option or a subcommand then an option from this app, you get a case insensitive one from now on.

### Extra arguments

Extra arguments are now available separately for subcommands and commands.

```cpp
CLI11_PARSE(app, argc, argv);

auto extra_args_app = app.remaining();
auto extra_args_sub = sub->remaining();
auto all_extra_args = app.remaining(true);
```

### Policies

You can now set several policies to control what happens if you receive the same single value option multiple times. For example, if you receive this on the command line:

```bash
$ ./my_app -x one -x two
```

By default this will throw an error if `x` takes a single argument. But, if you set `->take_last()`, you will get `"two"`, if you set `->take_first()` you will get `"one"`, and if you set `->join()` you will get `"one\ntwo"`. Using option default as described above, you can set this once at the beginning of your app.

### Failure messages

Failure messages now can be set with a function, `->set_failure_message(CLI::FailureMessage::help)` (the old default). The new default is much cleaner and more like most CLI programs. You can also add your own with a lambda function. You can also add something to the bottom of the help print with `->set_footer("...")`. Subcommands can also be grouped like options.

### Smaller features

You can now exit callbacks without a help message using `CLI::RuntimeError(int exitcode)`. `->require_subcommand` now has a two argument form that allows you to set min and max allowed commands (0 for unlimited). `CLI::ArgumentMismatch` provides a nicer error if arguments are missing. You can remove any option from config files with `->configurable(false)`. You can add a `string(string)` function that can pre-modify a command line argument with `->transform(func)`; this could be used to make paths absolute, clean up extra letters or brackets, etc.

See the [release notes][releases] for more details, checkout the [readme][CLI11] and the slowly expanding [GitBook tutorial series][GitBook], and update today!

 CLI11 was developed at the [University of Cincinnati] to support of the [GooFit] library under [NSF Award 1414736].


[GooFit]: https://github.com/GooFit/GooFit
[CLI11]: https://github.com/CLIUtils/CLI11
[releases]: https://github.com/CLIUtils/CLI11/releases
[API docs]: https://cliutils.github.io/CLI11
[NSF Award 1414736]: https://nsf.gov/awardsearch/showAward?AWD_ID=1414736
[University of Cincinnati]: https://uc.edu
[GitBook]: https://cliutils.gitlab.io/CLI11Tutorial
