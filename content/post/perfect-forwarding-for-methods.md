---
title: Perfect forwarding for methods
date: 2017-03-17T10:43:00.000-07:00
lastmod: 2017-03-17T10:43:01.614-07:00
categories:
  - cpp
tags:
  - programming
  - cpp
---

I often see perfect forwarding listed for constructor arguments, but not usually for functions with a return or methods. Here is my solution for an method method of class `Cls`:

```cpp
template<typename ...Args>
static auto method(Cls* cls, Args &&  ...args)
  -> typename std::result_of<decltype(&Cls::method)(Cls, Args...)>::type {
    return cls->method(std::forward<Args>(args)...);
}
```

This is useful if you want to call protected classes from a “helper” friend class, for example, to expose them to tests without having to require GoogleTest/GoogleMock to be available for regular users.

<!--more-->
