---
title: "ROOT Install Options"
subtitle: "New Conda Forge package of ROOT for Unix and more options"
date: 2019-02-04T10:44:37+01:00
lastmod: 2019-02-13T16:25:00+01:00
categories:
  - Python
tags:
  - programming
  - python
  - physics
  - root
---

For particle physicists, ROOT is one of the most important toolkits around. It is a huge suite of tools that predates the C++ standard library, and has almost anything a particle physicist could want. It has driven developments in other areas too. ROOT's current C++ interpreter, CLING, is the most powerful C++ interpreter available and is used by the Xeus project for Jupyter. The Python work has helped PyPy, with CPPYY also coming from ROOT. However, due to the size, complexity, and age of some parts of ROOT, it can be a bit challenging to install; and it is even more challenging when you want it to talk to Python. I would like to point to the brand-new Conda-Forge ROOT package for Linux and macOS, and point out a few other options for macOS installs. Note for Windows users: Due to the fact that ROOT expects the type `long` to match the system pointer size, 64-bit Windows cannot be supported for quite some time. While you can use it in 32 bit form, this is generally impossible to connect to Python, which usually will be a 64-bit build.

<!--more-->

## Linux

### Conda-Forge

Also see [my new post here](../root-conda) for more details!

Thanks mostly to the excellent work of Chris Burr, you can finally download ROOT as a conda package! You'll want to use the conda-forge channel. Make sure your conda is up to date; there have been lots of improvements to the base conda environment management and the compiler packages that make this possible. If you find any bugs, please report them to [The ROOT feedstock](https://github.com/conda-forge/root-feedstock) and we will get them fixed as quickly as possible. This is full ROOT; you
don't have to use PyROOT. Though, as a reminder for building by hand with conda, be sure to use `$CXX` to build instead of `g++`, since conda does not symlink in those common compiler names. Things like CMake will work properly by default. You have C++17 support and GCC 7+ with conda on Linux!

Currently, there is just one fully featured ROOT package; there might be a ROOT-minimal package in the future. External packages are used when possible, but CLANG, CLAD, and xxhash are still internal (partially due to the ROOT build system not allowing them to be external) - when this changes in ROOT, this will be fixed in the conda formula as well. We currently do not add optional integrated packages, ~~like Pythia~~[^1], but PRs and/or new formulas are happily accepted. ~~We hope to get macOS
support working soon.~~[^2] Windows support will only come if ROOT starts supporting 64-bit Windows builds, which is still a long way off to the best of my knowledge.

You can install it into the current environment like this:

```bash
conda install -c conda-forge root
```

Or, you can make a new environment with ROOT in it like this:

```bash
conda create -n myrootenv python=3.7 -c conda-forge root
```

Of course, you can list any other packages you want preinstalled, including the anaconda meta-package (that's the same as listing all 100 or so packages in the base anaconda distribution).

### Build it yourself

You can also [build ROOT yourself](https://root.cern/building-root). You'll want to get all the prerequisites [listed here](https://root.cern/node/2110). This is my collection of options for CentOS 7:

```bash
cmake -DCMAKE_INSTALL_PREFIX=/opt/root-6.16.00 \
      -Dpythia8=ON -Droofit=ON -Dmathmore=ON -Dminuit2=ON \
      -Dgdml=ON -DPYTHIA8_INCLUDE_DIR=/opt/pythia8240/include
      -DPYTHIA8_LIBRARY=/opt/pythia8240/lib/libpythia8.so \
      -Dimt=ON -Dcuda=ON -Dveccore=ON -Dtmva-gpu=ON ../root
```

It's a good idea to set  `-Dcxx11=OFF -Dcxx14=ON` if you can, or even up to 17 if you can handle that.

## macOS

For each of the options, there are three things to think about:

1. What Python interpreter does ROOT support? (Sometimes, you can get away with matching versions but different installs, such as system Python 2.7 and a separate 2.7 install, but that's not ideal). If you don't every plan to use PyROOT, you can ignore this, otherwise, it's the most important factor in picking a way to install.
2. What options are enabled when ROOT compiled, and if you can rebuild if you need to with different options.
3. Where it goes so you can find it and activate it. Usually you will be looking for `thisroot.sh` so you can source it - then all of the command line tools work properly.

Obviously, the version matters too, but all the methods I will outline are quite recent.


### The official install

We first have to talk about the elephant in the root. ROOT does provide installers for ROOT on several systems, including macOS. This however, is not my preferred method to install it on macOS and I finally deleted my "Official" install a few months ago. Let's cover the three items:

1. PyROOT is attached to your system Python, Python 2.7. On all systems, it is not recommended that you use your pre-installed system Python to do scientific work, since it's too easy to break it by trying to install things. It is much, much better to use a separate version. This is especially important for Apple's Python on macOS; since Apple may use it for system tasks, and it may even be customized by them if needed, and they include (an old) NumPy that probably is unusable in a modern stack. And this traps you into the dying Python 2 even on a modern system.
2. While this is "official", I seemed to find it was a bit light on activated options. Maybe it's better now, but you might have missing options still. And, there's no easy way to add an option and recompile, you'll have to start fresh from source.
3. This gets installed to `/Applications/root...`, which is "mac-like", but really odd for a program that you do not open from the GUI, but from the command line. You have to source `thisroot.sh` to use it.

Install instructions: See the webpage, it's just a normal mac installer package.

### Homebrew

This is my favorite package manager for macOS, and one of my top three favorite things about macOS. It beats all the linux package mangers in speed, customizability, and simplicity. It's like having a command-line mac App Store full of open source software. If you don't have it installed already, it's a must for any mac (IMHO). All the continuous integration (CI) systems that support macOS, like Travis, Azure, and CircleCI, have homebrew preinstalled. If you don't already have it, you can copy and paste one line from <https://brew.sh> to install it. I probably have several dozen packages installed from brew. This is the version of ROOT I use, and I am one of the package maintainers.

1. PyROOT is attached to the HomeBrew Python, which is Python 3.7 (HomeBrew is usually only a few days behind any new software release). This is not a bad choice for working with, since if you install something that breaks Python, it will only break your HomeBrew packages and not your system. I prefer to work with Anaconda (see below), but for ROOT's sake, I have enough installed on this version of Python to get by. I also have Jupyter installed with this Python so I can easily start up notebooks without activating Anaconda.
2. This has a nice rich set of options. And, it's completely trivial to add more, as long as you are very patent to wait for a root build. Just run `brew edit root` to see the complete formula used to build ROOT!
3. This is installed to `/usr/local/Cellar/root/6.16.00`, and then symlinked into `/usr/local`, which is a standard location, so it works even without ``thisroot.sh`` (though you can source that if you like).

Install instructions:

* Get HomeBrew if you don't have it from <https://brew.sh>
* Run `brew install root`.
* Done.
* Run `brew upgrade` to upgrade all your packages occasionally

Note: Brew even can install "Casks", which are non-source packages - allowing it to install inkscape, iterm2, and more!

### MacPorts

This is the other big package manager for macOS. I found it doesn't play well with brew, mostly because it uses the same locations (so you have to pick one manager), and it's much less transparent and simple. If you already use it though, it does have a nice ROOT package. At least one person from LHCb is a maintainer on the recipe. Embarrassingly, there was a bug found and patched in ROOT on macOS by the macPorts team six months ago that I am just now fixing in the HomeBrew formula and ROOT itself. I don't use macPorts though, so can't say much about it other than ports install root or something similar should work.

### Anaconda

The conda-forge package is now available for ROOT! You can simply write `conda install -c conda-forge root` (or, even better, use conda create to make an environment) and you'll get a full featured ROOT install, attached to your Anaconda Python! And, since it's part of the conda system, you can have multiple environments with multiple versions of Python, each with working ROOT! This is really exciting for people who use Anaconda Python as their main Python; for everything but ROOT (historically), it is the #1 way to work. It has faster NumPy due to their Intel connections, it has CUDA due to their NVIDIA connections, you can install all the big Machine Learning packages on it, etc.

See [my post here](../root-conda) for more details!

[^1]: Since this post, I have made Pythia8 available on Conda-Forge, and it is now enabled in ROOT.
[^2]: macOS ROOT on Conda-Forge was released after this post was published, and is now available.
