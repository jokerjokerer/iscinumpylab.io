---
title: "CMake 3.11"
date: 2018-04-02T11:16:57+02:00
categories:
  - cmake
tags:
  - programming
  - cmake
---

CMake 3.11 was just released; this is particularly exciting release for CMake. I'd like to give a quick and friendly introduction to the new features that might make the largest difference for CMake users.

<!--more-->

## Faster

CMake 3.11 is [supposed to be faster](https://blog.kitware.com/improving-cmakes-runtime-performance/). For large projects, every little bit counts.

## IMPORTED INTERFACE

CMake has two kinds of interface targets. A regular INTERFACE library is something you still "own", so you need to export it when you export your interface. An IMPORTED INTERFACE is something you don't own, so anyone using your exported interface needs to import it (make it) too. If you wrap an old-style `find_package` variable set into an interface, you want to make it IMPORTED, and if you write a `Find*.cmake` file, you'll be making IMPORTED INTERFACE targets.

However, building IMPORTED INTERFACE targets is much harder than INTERFACE targets, since you have to set properties directly; the `target_add_*` commands oddly did not support IMPORTED INTERFACE targets. Now they do.

{{< columns >}}

## CMake 3.10

```cmake
add_library(Interface INTERFACE)
add_library(Imp::Inter IMPORTED INTERFACE)

target_include_directories(Interface INTERFACE include)
set_property(TARGET Imp::Interface
             PROPERTY INTERFACE_INCLUDE_DIRECTORIES include)
```

{{< column >}}

## CMake 3.11

```cmake
add_library(Interface INTERFACE)
add_library(Imp::Inter IMPORTED INTERFACE)

target_include_directories(Interface INTERFACE include)
target_include_directories(Imp::Inter INTERFACE include)
```

{{< endcolumns >}}

## FetchContent

This is another long-desired feature for many CMakers. It takes the build-time download features and provides a configure-time version! This means that you can download data or other projects as part of your configure step. Things like `add_subdirectory` now work, since the directories exist as soon as the content is fetched. It replaces projects like [DownloadProject](https://github.com/Crascit/DownloadProject).

```cmake
include(FetchContent)
FetchContent_Declare(
    googletest
    GIT_REPOSITORY https://github.com/google/googletest.git
    GIT_TAG        release-1.8.0
)

FetchContent_GetProperties(googletest)
if(NOT googletest_POPULATED)
    Fetch`Content_Populate(googletest)
    add_subdirectory(${googletest_SOURCE_DIR} ${googletest_BINARY_DIR})
endif()
```

## Smaller features

Compile language generator expressions are now supported in Visual Studio and Xcode; this was added to improve CUDA support, since it requires COMPILE_LANGUAGE generator expressions to select CUDA vs. C++ code. You can also add more properties to source files, making them more like targets in the inheritance system.
