---
title: Including CRY cosmic ray generator in CMake
date: 2015-10-19T09:07:00.002-07:00
lastmod: 2015-11-19T09:47:52.181-08:00
categories:
  - Physics
tags:
  - programming
  - cosmic-rays
---

I realized that CRY did not have a CMake based install option, so including it in a GEANT4 cmake project might not be obvious. This is how you would do it in your CMakeLists.txt:

<!--more-->

```cmake
# CRY
include(ExternalProject)
ExternalProject_Add(
    CRY-1.7
    URL http://nuclear.llnl.gov/simulation/cry_v1.7.tar.gz
    URL_MD5 85f240bebe81fe0b257e92ef1d390a83
    CONFIGURE_COMMAND ""
    INSTALL_COMMAND ""
    BUILD_IN_SOURCE 1
    TEST_AFTER_INSTALL 1
)

ExternalProject_Get_Property(CRY-1.7 source_dir)
set(CRY_INCLUDE_DIRS "${source_dir}/src")
set(CRY_LIBRARY_DIR "${source_dir}/lib")
set(CRY_LIBRARIES "libCRY.a")
include_directories(${CRY_INCLUDE_DIRS})
link_directories(${CRY_LIBRARY_DIR})
set(LINK_LIBS ${CRY_LIBRARIES} ${LINK_LIBS})
```


Hope that helps! As this is a Python blog, here's a CMake based version of CRY with Python bindings: https://bitbucket.org/mayamuon/cry
