---
title: "Should You Use Upper Bound Version Constraints?"
date: 2021-11-09T12:00:00-04:00
categories:
  - Python
tags:
  - programming
  - python
hidden: true
---

Bound version constraints (upper caps) are starting to show up in the Python
ecosystem. This is causing [real world problems][pyhf tensorflow issue] with
libraries following this recommendation, and is likely to [continue to get
worse][packaging pyparsing issue]; this practice does not scale to large
numbers of libraries or large numbers of users. In this discussion, I would
like to explain the difference between an application and a library, why
_always_ providing an upper limit causes far more harm than good *even for
SemVer libraries*, why libraries that pin upper limits require *more frequent*
updates rather than less, and why it is not scalable.  After reading this,
hopefully you will know the (few) places where pinning an upper limit is
reasonable, and will possibly even avoid using libraries that pin upper limits
needlessly until the author updates them to remove these pins.

If this behemoth is a bit long for you, then skip around using the table of
contents, or see the TL;DR section at the end. Also be warned, I pick on
[Poetry][] quite a bit. The rising popularity of Poetry is likely due to the
simplicity of having one tool vs. many for packaging, but it happens to also
have a special dependency solver, a new upper bound syntax, and a [strong
recommendation to always limit upper versions][poetry version constraints] - in
direct opposition to [members of the Python core developer team][why i don't
like semver] and PyPA developers. Not all libraries with excessive version
capping are Poetry projects (like TensorFlow), but many, many of them are. To
be clear, Poetry doesn't force version pinning on you, but it does push you
really, really hard to always version cap, and it's targeting new Python users
that don't know any better yet than to accept bad recommendations.  I do really
like other aspects of Poetry, and would like to [eventually][scikit-build post]
help it build binary packages with Scikit-build (CMake) via a plugin, and I use
it on some of my projects happily.

[poetry]: https://python-poetry.org
[poetry version constraints]: https://python-poetry.org/docs/faq/#why-are-unbound-version-constraints-a-bad-idea
[pyhf tensorflow issue]: https://github.com/pypa/packaging/pull/482#issuecomment-963030131
[why i don't like semver]: https://snarky.ca/why-i-dont-like-semver/
[packaging pyparsing issue]: https://github.com/pypa/packaging/pull/482#issuecomment-963030131
[scikit-build post]: https://iscinumpy.gitlab.io/post/scikit-build-proposal

<!--more-->

This turned out to be 14 pages long, so I've included a table of contents. Feel
free to jump to the thing that you care about.

{{< toc summary="**Table of Contents** (click to expand)" />}}

# Intro

## Definitions

Before we continue, we need to define a few terms. First, let's clarify
**Application** vs. **Library**. While the actual distinction between these
common terms might be muddled in practice, I will be using a very specific
definition for our discussion below.

### Library

A library is something a user can import as part of a larger project, either
another library or an application. The defining feature is that a library
**cannot dictate what other packages live alongside it**. A user might use your
library, but they also may need to use other libraries as well, and as a
package author, you can't control what they use (though you can make version
constraints on a known subset of dependencies). Pip does not allow you to make
version constraints on packages you do not require, though Conda does.

### Application

This is a package that is intended to be used directly by a user, and **is not
required to live in an environment with arbitrary libraries**. Many users will
not understand packaging well enough to install it in a new virtual
environment, and will instead add it to an existing environment, or even their
system environment, it it _could_ be installed in isolation, say with [pipx][].
It might not even be PyPI installable, but instead only have a "development"
style install, ideally with a lock file (web applications often fall into this
category). It will almost always have a terminal interface or graphical user
interface, and may not support being imported from Python at all.

A package _can_ be both; sometimes a package has a library interface and
application interface. That's fine; it just means that both rules apply.

Also, a **framework** is a package that usually is a library, but can possibly
be an application - it may be intended to be "complete", allowing a user to
interact with it by writing a Python file without importing any extra packages.
However, in almost all cases, it's really, really helpful to be able to import
other packages, making it much nicer to treat as a library instead of an
application. Many frameworks (like basically all web frameworks) are
"incomplete", which makes them fully intended to be libraries.

### SemVer

Now, let's define SemVer. This states there are three version digits in the
form `Major.Minor.Patch`. The "rule" is that only fixes are allowed if the
patch number is increased, only additions are allowed if the minor version is
bumped, and if you do anything could break downstream users, then the major
version must be bumped.

Whenever any downstream code breaks and it was not a major release, then you'll
have a smattering of people that immediately start complaining that the library
"didn't follow SemVer", and that it was not a problem with SemVer, but your
problem for not following it. [A long discussion can be found
here][why i don't like semver], but I'll give a tiny taste of it. Which
version do you bump if you add a new error? How about if it's just a warning?
It turns out one person's bugfix is another's breaking change.  Basically, a
"perfect" SemVer library would pretty much always bump the major version, since
you almost always could possibly break a user with any change (and if you have
enough users, by [Hyrum's law][], you will do this).  This makes "true" SemVer
pointless. Minor releases are impossible, and patch releases are nearly
impossible. If you fix a bug, someone could be depending on the buggy behaviour
(distutils, looking at you). Of course, even a SemVer purist will admit users
should not break if you add or fix something, but that does mean there is no
such thing as "pure" SemVer. PyParsing recently removed a non-public attribute
in 3.0.5, which broke all released versions of packaging, since it happened to
be using this non-private attribute. Was this packaging's fault? Yes, but now
pyparsing 3.0.5 is a breaking release for a _lot_ of the Python ecosystem.

[hyrum's law]: https://www.hyrumslaw.com

Does dropping Python 2 require a major release? Many (most) packages did this,
but the general answer is probably not; the version solver will ensure the
correct version is used (unless the `Requires-Python` metadata slot is empty or
not updated, _never_ forget this, never set lower than what you test!).

Now, don't get me wrong, I love "realistic" or "almost" SemVer - I personally
use it on all my libraries (I don't maintain a single CalVer library like pip
or only-live-at-head library, like googletest). Practical SemVer mostly follows
the rule above, but acknowledges the fact that it's not perfect. It also often
adds a new rule to the mix: if you deprecate a feature (almost always in a
minor release), you can remove that feature in a future minor release. You have
to check the library to see what the deprecation period is - NumPy and Python
use three minor releases. Really large libraries hate making major releases -
Python 2->3 was a disaster. SemVer purists argue that this makes minor releases
into major releases, but it's not that simple - the deprecation period ensures
the "next" version works, which is really useful, and usually gives you time to
adjust before the removal happens. It's a great balance for projects that are
well kept up using libraries that move forward at a reasonable pace. If you
make sure you can see deprecations, you will almost always work with the next
several versions.

The best description of realistic SemVer I've seen is that it's an "abbreviated
changelog". I love this, because I love changelogs - I think it is the most
important part of documentation you have, a [well written
changelog][keepachangelog] lets you see what was missing before so you know not
to look for it in older versions, it lets you know what changed so you can
update your code (both to support a version as well as again when you drop
older versions), and is a great indicator of the health and stability of a
project. With SemVer, you can look at the version, and that gives you a quick
idea of how large and what sort of changes have occurred before checking the
changelog.

[keepachanglog]: https://keepachangelog.com

## Solver

We need to briefly mention the solver, as there happen to be several, and one
reason this is more relevant today than a few years ago is due to changes in
the solver.

Pip's solver changed in version 20.2 to become significantly smarter. The old
solver would ignore incompatible transitive requirements much more often than
the new solver does. This means that an upper cap in a library might have been
ignored before, but is *much* more likely to break things now.

Poetry has a unique and very strict (and slower) solver that goes even farther.
Besides trying to find a set of dependencies that match, it will also make sure
you have the same requirements locally too (at least on Python). So if a single
dependency puts `python<4` or `python=^3.6`, then you are forced to limit
Python's upper limit in your package metadata too.

Conda's solver is like Poetry, and this should scare you - initial solves for
an environment with a large number of dependencies (including a single package
like ROOT) can take minutes, and updates to existing environments can take more
than a day.  I've never had Poetry take more than 70 seconds, but I've also not
used it on anything large. Conda has gotten better by taking more shortcuts and
guessing things (I haven't had a 25+ hour solve in a while), and Mamba's C
implementation and better algorithms _really_ help, but doing a "smart" solve
is hard.

We'll see it again, but just to point it out here: solver errors are pure evil,
and can't be fixed. If a library requests `pyparsing>=3` and another library
requests `pyparsing<3`, that's the end, you are out of business. "Smart"
solvers may look for older versions of those libraries to see if one exists
that does not have that cap - if the one with the high lower bound had a
release with a lower upper bound, that's what it will choose; regardless of
what bugs have been fixed, etc. since that release. We'll discuss the problems
this causes later. However, an under-constrained build is completely trivial to
fix.


# The problem: Relying on SemVer for capping versions

Now comes the problem: If you have a dependency, should you add an upper cap?
Let's look at the different aspects of this.

We'll cover the valid use cases for capping after this section. But, just to be
clear, if you **know** you do not support a new release of a library, then
absolutely, go ahead and cap it as soon as you know this to be true. If
something does not work, you should cap (or maybe restrict a single version if
the upstream library has a temporary bug rather than a design direction that's
causing the failure). You should also do as much as you can to quickly remove
the cap, as all the downsides of capping in the next section still apply.

The following will assume you are capping _before_ knowing that something does
not work, but just out of general principle, like Poetry recommends and
defaults to with `poetry add` and the default template. In most cases, the
answer will be "don't". For simplicity, I will also assume you are being
tempted to cap to major releases (`^1.0.0` in Poetry or `~=1.0` in all other
tooling that follows Python standards via [PEP 440][]). If you cap to minor
versions (`~=1.0.0`), this is much worse, and the arguments below apply even
more strongly.

## Version limits break code too

No one likes having an update break users. For example, Jedi 0.18 removed
something that IPython used, so until IPython 7.20 was released, a
"normal" solve (like `pip install ipython`) resulted in a broken install. It's
tempting to look back at that and say "well, if IPython capped it's
dependencies, that would have been avoided". However, capping dependencies also
breaks things, and you _can't fix it downstream_. If I write a library or
application that depends on a library that has a broken dependency, I can limit
it, and then my users are happy. In the case above, the interim solution was to
just manually pin jedi, such as `pip install ipython jedi<0.18` for a user or
to cap it in dependencies for a library or application. Any user can easily do
that - irritating, and a leaky abstraction, but fixable. But you *can't* fix an
over-constraint - and this is not just a pip issues; you'd have to throw out
the entire dependency solver system to get things to install.

If you put upper limits, this also then _can't easily_ be fixed by your
dependencies - it usually _forces the fix on the library that does the
pinning_. This means every single major release of every dependency you cap
immediately requires you to make a new release or everyone using your library
can no longer use the latest version of those libraries. If "make a new release
quickly" from above bothered you; well, now you have to make it on every version
bump of every pinned dependency.[^1]

[^1]: One common pushback here is that a smart dependency solver will get old
  versions, so updating is not pressing. But new libraries shouldn't have to
  support really old versions of things just because they can't live with
  libraries with old caps. Libraries shouldn't have to keep pushing updates to
  old major/minor releases to support new hardware and Python versions, etc. So
  yes, you are "promising" to update rapidly if capped dependencies update.
  Otherwise, your library cannot be depended on.

It also means you must support a wide version range; ironically, this is the
very opposite of the syntax Poetry adds for capping. For example, let's say you
support `click^7`. Click 8 comes out, and someone writes a library requiring
`click^8`. Now your library _can't be installed_ at the same time as that other
library, your requirements do not overlap. If you update to requiring `click
^8`, your update can't be installed with another library still on `click^7`. So
you have to support `click>=7,<9` for a while until most libraries have
similarly updated (and this makes the `^` syntax rather useless, IMO). This
particular example is especially bad, because a) it's common, b) click is used
for the "application" part of the code, which is likely not even used by your
usage if you are using it as a library, and c) the main breaking change in
Click 8 was the removal of Python<3.6 support.

You want users to use the latest versions of your code. You want to be able to
release fixes and have users pick up those fixes. And, in general, you don't
like having to release new patch versions for old major (or even minor)
versions of your library. So why force your dependencies to support old
versions with patch releases because you've capped them?

## Code does not work forever

While it seems to be removed, one claim I've seen Poetry make is that "capping
your dependencies means your code will work forever". Hopefully I don't have to
tell you this is wrong, and even Poetry has removed the claim. There are lots
of reasons code breaks without changing it. One of the most recent ones was the
macOS Apple Silicon transition. In order to get working code, you have to have
the latest versions of packages.  Python 3.9 (and later backported to 3.8) is
required on Apple Silicon, so if you depend on a library capped to not support
those versions, you are out of luck, that code does not work. This is also true
with all the major libraries, which did not backport Apple Silicon support very
far. You need a recent NumPy, pip, packaging, poetry, etc. Similar support
rollouts have happened (or are happening) for Linux and Windows architectures
(like ARM and PowerPC), operating system updates (macOS 11's new numbering
system broke pip and lots of other things, might also happen to a lesser extent
on Windows 11), new manylinux versions, PyPy support, Musllinux wheels, and
even just adding wheel support in general, actually. Code simply will never
work forever, and allowing the possibility of using newer libraries increases
the chance it can be used.  If you limit to NumPy to 1.17, it will never
support Apple Silicon. However, if you don't limit it, and the final version
you support happens to be 1.21, then your library will work with Apple Silicon,
and future users may have to manually limit versions to some unknown maximum
version someday, _but it will work_. Artificially limiting versions will
_always_ reduce the chances of it working in the future. It just avoids users
in the future from having to add extra limits, but this is a problem that has a
user workaround, and is honestly not that likely to happen for many
dependencies. If someone is using a multiple-year old version of your library,
either you disappeared (and you therefore can't fix broken upper limits), or
they are being forced to use an old version, probably because someone someone
else (artificially) pinned your library.

Another problem is with broken releases. Let's say 6.1.0 worked. You pin to
`=^6.1.0` or `~=6.1`. Then `6.2.0` comes out, and breaks your code. The problem
is discovered and fixed, but the development has gone on too far to easily
backport, or it's too involved, so `7.0.1` works again. Your cap is now broken,
and your code _does not_ work forever. I have seen fixes like this multiple
times, and have been responsible for them, as well. Often the CI system breaks
for old, unmaintained releases, and it's not feasible to go back and fix the
old version - you just point people at the new version. But as mentioned,
capping breaks the ability to do this! You are literally in the business of
pushing updates if you are reading this. Limiting the ability for anyone else
to push updates should really bother you.

## SemVer never promises to break your code

A really easy but incorrect generalization of the SemVer rules is "a major
version will break my code". It's the basis for Poetry's recommendation to
always cap versions, but it's a logical fallacy. Even if the library follows
perfect SemVer, *a major version bump does not promise to break downstream
code*. It promises that *some* downstream code *may* break. If you use pytest
to test your code, for example, the next major version will be _very_ unlikely
to break. If you write a pytest extension, however, then the chances of
something breaking are much higher (but not 100%, maybe not even 50%).

As a general rule, if you have a reasonably stable dependency, and you only use
the documented API, especially if your usage is pretty light/general, then a
major update is extremely unlikely to break your code. It's quite rare for
light usage of a library to break on a major update. It _can_ happen, of
course, but is unlikely. If you are using something very heavily, if you are
working on a framework extension, or if you use internals that are not publicly
documented, then your chances of breaking on a major release are much higher.

## It doesn't scale

If you have a single package that doesn't play well, then you probably will get
a working solve easily. If more packages start following this tight capping,
however, you end up with a situation where things simply cannot solve. The
entire point of packaging is to allow you to get lots of packages that each do
some job for you - we should be trying to make it easy to be able to add
dependencies, not harder.

The implication of this is you should be very careful when you see tight
requirements in packages and you have any upper bound caps anywhere in the
dependency chain.  If something caps dependencies, there's a very good chance
adding two such packages will break your solve, so you should pick just one (or
avoid them altogether, so you can add one in the future).

## It conflicts with tight lower bounds

A tight lower bound is only bad if packages cap upper bounds. If you can avoid
upper-cap packages, you can accept tight lower bound packages, which are much
better. A good packaging system should allow you to require modern packages;
why develop for really old versions of things if the packaging system can
upgrade them? But a upper bound cap breaks this. Hopefully anyone who is
writing software and pushing versions will agree that tight lower limits are
much better than tight upper limits, so if one has to go, it's the upper
limits.

It's also rather rare that packages solve for lower bounds in CI (I'd
love to see such a solver become an option, by the way!), so setting a tight
upper bound is one way to avoid rare errors when old packages are cached that
you don't actually support. CI almost never has a cache of old packages, but
users do.

Please test with a `constraints.txt` file that forces your lower bounds, by the
way, at least if you have a reasonable number of users.

## Capping dependencies hides incompatibles

Another serious side effect of capping dependencies (this one really applies
equally to applications and libraries) is that you are not notified properly of
incoming incompatibilities, and you have to be extra proactive in monitoring
your dependencies for updates. If you don't cap your dependencies, you are
immediately notified when a dependency releases a new version, probably by your
CI, the first time you build with that new version. If you are running your CI
with the `--dev` flag on your pip install (uncommon, but really probably a good
idea), then you might even catch and fix the issue before a release is even
made. If you don't do this, however, then you don't know about the
incompatibility until (much) later. If you are not following all of your
dependencies, you might not notices you are out of date until it's both a
serious problem for users and it's really hard for you to tell what change
broke your usage because several versions have been released. While I'm not a
huge fan of Google's live-at-head philosophy (primarily because it has heavy
requirements not applicable for most open-source projects), I appreciate and
love catching a dependency incompatibility as soon as you possibly can; the
smaller the changeset, the easier it is to identify and fix the issue.

## Capping all dependencies hides real incompatibilities

If I see `X>=1.1`, that tells me that you are using features from 1.1 and do
not support 1.0. If I see `X<1.2`, this _should_ tell me that there's a problem
with 1.2 and the current software. Not that you just capped all your
dependencies and have no idea if that will or won't work at all. A cap should
be like a TODO; it's a known issue that needs to be worked on soon.


## Pinning the Python version is special

Anther practice pushed by Poetry is adding an upper cap to the Python version.
This is misusing a feature designed to help with dropping old Python versions
to instead stop new Python versions from being used. "Scrolling back" to find
the newest version that does not restrict the version of Python being used is
exactly the wrong behavior for an upper cap, and that is what the purpose of
this field is. Current versions of Pip do seem to fail when this is capped,
rather than scrolling back to find an older uncapped version, but I haven't
found many libraries that have "added" this after releasing to be sure of that.

To be clear, this is very different from a library; specifically, **you can't
downgrade your Python version**[^2] if this is capped to something below your
current version. You can only fail. So this does not possibly "fix" something
by getting an older, working version, *it only causes hard failures* if it
works the way you might hope it does. This means instead of seeing the real
failure and possibly helping to fix it, users just see a Python doesn't match
error. And, most of the time, it's not even a real error; if you support Python
3.x without warnings, you should support Python 3.x+1 (and 3.x+2, too).

Capping to `<4` (something like `^3.6` in Poetry) is also directly in conflict
with the Python developer's own statements; they promise the 3->4 transition
will be more like the 1->2 transition than the 2->3 transition. It's not likely
to happen soon, and if it does, it likely will be primarily affecting Stable
ABI / Limited API builds and/or GIL usage; it likely will not affect normal
Python packages more than normal updates will. If you write

And, if you use Poetry, as soon as someone caps the Python version, every
Poetry project that uses it must also cap, even if you believe it's a
detestable practice and confusing to users. It's also wrong unless you fully
pin the dependency that forced the cap - if the dependency drops it and you
support the update that they released the drop in, you no longer would need the
cap.

If you are a package like Numba, where internal Python details (bytecode) are
relies on so there really is a 0% chance of it working, capping here is
acceptable. Otherwise, never provide an upper cap to your Python version. I
generally will not use a library that has an upper cap to the Python version;
when I have missed this, I've been [bitten by
it](https://github.com/hadialqattan/pycln/pull/81), hard
([cibuildwheel](https://github.com/pypa/cibuildwheel/pull/886),
[pybind11](https://github.com/pybind/pybind11/pull/3397), and several other
package's CI went down). To be clear, in that case, Python 3.10 was perfectly
fine, and you could install a venv with 3.9 and then upgrade to 3.10 and it
would still work. It just broke installing with 3.10.

[^2]: In pip or Poetry. Conda can do this, because Python is more like a
  library there. But we aren't discussing conda, and at least conda-forge has
  it's own system, and it's not tied to your normal packaging config at all,
  the package names may not even be the same, etc.

## Applications are different

Now if you have a true application (that is, if you are not intending your
package to be used as a library), upper version constraints are much less
problematic. However, you should _never depend only on SemVer_ for a library. I
won't repeat the [above article][why i don't like semver] verbatim here, but in
general, you are roughly as likely to get a breakage (usually unintentional)
from a minor or patch release of a library than from a major version. Depending
on the stability and quality of the library, possibly more likely. So
applications only have one choice: They should supply a lock file that has every
dependency explicitly listed. All systems have the ability to do this - you can
use pip-tools for pip, Poetry makes lock files automatically, etc. This gives
users a way to install using exactly the known working dependencies. In
production (say for a website), you _must_ do this. Otherwise, you will
randomly break. This is why patch releases exist, it's because a major, minor,
or even other patch release broke something!

If you are not using Poetry or pip-tools, you can still make a simple lock file
with:

```console
pip freeze > requirements-lock.txt
```

Then you can install it with:

```console
pip install --no-deps -r requirements-lock.txt
```

While this does not include hashes like Poetry, pipenv, or pip-tools will, it
covers many low-risk use cases, like setting up a simple web application.

What about your general requirements that control the locking process? With a
lockfile, you'll know when you try to update it that something breaks, and then
you can add a (temporary) pin for that dependency. Adding arbitrary pins will
reduce your ability to update your lock file with the latest dependencies, and
obscure what actually is not supported with what is arbitrarily pinned.


# Upper limits are valid sometimes

## When is it okay to set an upper limit?

Valid reasons to add an upper limit are:

1. If a dependency is known to be broken, cap just below the broken version.
   Try very hard to fix this problem quickly, then remove the cap. If the fix
   happens upstream, change to excluding just the broken version.
2. If you know upstream is about to make a major change that is very likely to
   break your usage, you can cap. But try to fix this as quickly as possible so
   you can remove the cap by the time they release. Possibly add development
   branch/release testing until this is resolved. TensorFlow 1-2, for example,
   was a really major change that moved things around. But fixing it was really
   as simple as importing from `tensorflow.v1`.
3. If upstream _asks_ users to cap, then I still don't like it, but it is okay
   if you want to follow the upstream recommendation. You should ask yourself: do
   you want to use a library that may intentionally break you and require changes
   on your part?
4. If you are writing an extension for an ecosystem/framework (pytest
   extension, Sphinx extension, Jupyter extension, etc), then capping on the
   major version of *that* library is acceptable. Note this happens once - you
   have a single library that can be capped. You must release as soon as you
   possibly can after a new major release, and you should be closely following
   upstream - probably using development releases for testing, etc. But doing
   this for one library is probably manageable.
5. You are releasing two or more libraries in sync with each other. You control
   the release cadence for both libraries. This is likely the "best" reason to
   cap. _Some_ of the above issues don't apply in this case - since you control
   the release cadence and can keep them in sync.
6. You depend on private internal details of a library. You should also rethink
   your choices - this can be broken in a minor or patch release, and often is
   (pyparsing 3.0.5, for example).

If you cap in these situations, I wouldn't complain, but I wouldn't really
recommend it either:

7. If you have a heavy dependency on a library, maybe cap. A really large API
   surface is more likely to be hit by the possible breakage.
8. If a library is very new, say on version 1, and has very few users, _maybe_
   cap if it seems rather unstable. See if the library authors recommend
   capping - they might plan to make a large change if it's early in
   development.
9. If a library looks really unstable, such as having a history of making big
   changes, then cap. Or use a different library. Even better, contact the
   authors, and make sure that your usage is safe for the near future.

All these are special cases, and are uncommon; no more than a few of your
dependencies should fall into the categories above. In every other case, do not
cap your dependences! You could probably summarize it like this: if there's a
high chance (say 75%+) that a dependency will break for you when it updates,
you can add a cap. But if there's no reason to believe it will break, do not
add the cap; you will cause more severe (unfixable) pain than the breakage
would.

Notice many of the above instances are due to very close/special interaction
with a small number of libraries (either a plugin for a framework, synchronized
releases, or very heavy usage). _Most_ libraries you use do not fall into this
category. Remember, _library authors don't want to break users who follow their
public API and documentation_.  If they do, it's for a special and good reason
(or it's a bad library to depend on). They will probably have a deprecation
period, produce warnings, etc.

If you do version cap anything, you are promising to closely follow that
dependency, update the cap as soon as possible, follow beta or RC releases or
the development branch, etc. When a new version of a library comes out, end
users should be able to start trying it out. If they can't, your library's
dependencies are a leaky abstraction (users shouldn't have to care about what
dependencies libraries use).

## Planned obsolescence

There's one more reason to add upper version caps that I'm not including above.
If you plan to eventually change the licence of your library and/or make it
closed source, then adding upper version caps will ensure that the old, open
source versions of your library will become obsolete quickly. Using upper
version caps forces your users to depend on you for frequent version updates.
If you stop and move to a different model, your code quickly becomes
uninstallable on newer Python versions, on newer hardware or OSs, or with newer
libraries. This often is transitive; a limit on package X and package X adds
support for Musllinux, but you can't access it because of the upper limit.

This _might_ happen eventually if you don't limit, but it will happen much
faster and with more assurance with hard limits.

## Examples of acceptable pins

Numba pins LLVMLight exactly, and puts a hard cap on Python (and recently,
NumPy too). They control both Numba and LLVMLight, so the pinning there is okay
(reason 5 above). They use Python bytecode to decompile Python functions; this
is an internal detail to Python, so every minor release is allowed to (does)
change bytecode, so Numba must support each version manually (reason 6 above).
They know the most recent version of NumPy is incompatible (reason 1 above). In
both cases, they also put a check in `setup.py`, but remember, that only
affects _building_ Numba, so that works for Python, but may not work for normal
dependencies, like NumPy since normally users install wheels, not SDists, so
setup.py does not run. Numba should (and will) release this pin as quickly as
possible, because there are quite a few reasons to use NumPy 1.21, including it
being the first NumPy to support Python 3.10.

I personally limit [hist][] to the minor release of [boost-histogram][]. I
control both packages, and release them in sync; a hist release always follows
a new minor release of boost-histogram. They are tightly coupled, but part of a
family (reason 1 above). At this point, boost-histogram is likely stable enough
even in internal details to ease up a bit, but this way I can also advertise
the new boost-histogram features as new hist features. ;)

Many packages follow Flit's recommendation and use `requires = ["flit_core
>=3.2,<4"]` in the pyproject.toml build specification. This is reason 3 above;
flit asks you to do this. It's also in the pyproject.toml, which by definition
will never be "shared" with anything, it's a new, disposable virtual
environment that is created when building a wheel, and then thrown away, making
it much more like an application requirement. However, if you only use the [PEP
621][] configuration for Flit, I see no reason to cap it; this is a published
standard and isn't going to change, so Flit 4 will not "break" usage unless a
bug is introduced.

## Examples of bad caps

### TensorFlow

Now let's look at a bad upper limit and the mess it caused. TensorFlow puts an
[upper cap on
everything](https://github.com/tensorflow/tensorflow/blob/5972e123114bdfb8c877a6746702b104022fe4d9/tensorflow/tools/pip_package/setup.py#L77-L108)
(note some of the comments there are wrong, for example, order does not matter
to the solve). This is a complete mess. Several of the dependencies here are
small little libraries that are not going to break anyone on updates, like
`wrapt` and `six`. Probably the worst of all though is `typing_extensions`.
This is a backport module for the standard library `typing` module, and it's
pinned to `3.7.x`. First, new versions of `typing_extensions` are not going to
remove anything at least for five years, and maybe not ever - this is a
compatibility backport (the stdlib `typing` might be cleaned up after 5 years).
Second, since this is a backport, setting a high lower bound on this is very,
very common - if you want to use Python 3.10 features, you have to set a higher
lower bound. Black, [for
example](https://github.com/psf/black/blob/41e670064063e3e221b1c3c40db5aaae784b5231/setup.py#L74-L87),
sets 3.10.0 as the minimum. This is completely valid, IMO - if you have a
backport package, and you want the backports from Python 3.10, you should be
able to get them. Okay, so let's say you run this:

```bash
python3 -m venv .venv
./.venv/bin/pip install black tensorflow
```

(or pretend that's in a `requirements.txt` file for a project, etc - however
you'd like to think of that). First, the resolver will download black 21.8b0.
Then it will start downloading TensorFlow wheels, working it's way back several
versions - if you are on a limited bandwidth connection, be warned each one is
several hundred MB. Eventually it will give up, and start trying older black
versions. It will finally find a set that's compatible, since older black
versions don't have the high pin, and will install that. Now try this:

```bash
python3 -m venv .venv
./.venv/bin/pip install black
./.venv/bin/pip tensorflow
```

This will force `typing-extensions` to be rolled back, and then will be broken with:

```
ERROR: pip's dependency resolver does not currently take into account all the packages that are installed. This behaviour is the source of the following dependency conflicts.
black 21.8b0 requires typing-extensions>=3.10.0.0, but you have typing-extensions 3.7.4.3 which is incompatible.
```

In other words, simply having black pre-installed will keep you from installing
TensorFlow, _even though they are completely unrelated_. The reason? TensorFlow
thinks it has to have `typing_extensions` 3.7 instead of 3.10, which is wrong.
With such strong pinning, TensorFlow is effectively an application, it cannot
play nicely with pretty much any other library.

Due to the problems this caused, Tensorflow has removed the upper caps on most
dependencies, and you can now install it again with other libraries.

### Packaging and PyParsing

Packaging is a foundational library for most of Python packaging. Everybody
either depends on it (tox, cibuildwheel, etc) or vendors it (pip, Poetry,
pipenv). Packaging has very few dependencies, but it doesn't require pyparsing.
In version 3, pyparsing changed the name of some tokens - but provided backward
compatible names. Packaging worked just fine with version 3, but it affected
the text of one error message that was being compared in the tests, so
packaging capped pyparsing to <3, and then released packaging 20.2 with no
other change (compared to 20.1) except this cap. This immediately started
braking things (like Google App Engine deployment, and other complains stating
"ton of dependency conflicts"). To be clear, it didn't solve anything except
one test inside packaging itself. Then PyParsing 3.0.5 was released with a
change to in internal method name (starting with an underscore). This was used
by packaging (bad), so the real limit was either `<3.0.5`, or `!=3.0.5`
(depending on whether pyparsing will be nice and restore this as a workaround).
The correct fix is to not use a private implementation detail.

# TL;DR

Only add a cap if a dependency is known to be incompatible or there is a high
(>75%) chance of it being incompatible in its next release. Do not cap by
default - capping dependencies makes your software incompatible with other
libraries that also have strict limits on dependencies. You can fix a missing
cap, you cannot fix an over restrictive cap. It also encourages hiding issues
until they become harder to fix, it does not scale to larger systems, and some
tools (Poetry) force these bad decisions on your downstream users if you make
them. Even perfect SemVer does not promise your usage will be broken, and no
library can actually perfectly follow SemVer anyway; minor versions and even
patch versions are just about as likely to break you as major versions for a
stable library. Provide an optional working set of fully pinned constraints if
that's important to you (common for an application).

If you set upper limits, you should release a new version as soon as possible
with a higher cap when a dependency updates (ideally before the dependency
releases the update). If you are committing to this, why not just quickly
release a patch release with caps only after an actual conflict happens? It
will be less common, and will help you quickly sort out and fix
incompatibilities, rather than hiding your true compatibilities and delaying
updates. You want users to use the latest versions of your libraries if there's
a problem, why can't you offer the same consideration to the libraries you
depend on and use?

# Final thoughts

Almost everything here is generalizable to similar ecosystems, not just Python,
though I have have focused on Python explicitly. It is _not_, however,
generalizable to systems that are able to provide unique versions to each
package - like Node.js. These systems can avoid resolver conflicts by providing
different versions to each package. This is very, very different from a system
that requires a single version. Those systems are also where the `^` syntax is
much more useful. Some tools (like Poetry) seem to be trying to apply part of
those systems (caret syntax, for example) without applying the local version
feature, which is key to how they work. Having local (per dependency) copies of
all dependencies solves many of the issues above, though some of the arguments
above _still_ apply, like hiding incompatibilities until the changeset is very
large.

---

# PS: Why so hard on Poetry?

I do rather like Poetry, and I provide it as one of seven backends for the
Scikit-HEP/cookie project generation cookiecutter. But I believe most users
don't realise it has a unique, slow, and opinionated solver. Also, Poetry users
are often intimidated by the plethora of tools it can replace, like
setuptools/flit, venv/virtualenv, pip, pip-tools, wheel, twine, bump2version,
and nox/tox; and that sort of user is very easily influenced by the defaults
and recommendations they are seeing, since they do not have enough experience
in the Python ecosystem to know when a recommendation is a bad one. Not only
does running `poetry add <package>` automatically use `^<latest>`, but
generating a new project adds both a caret cap to pytest and Python itself! And
if you have `python = ^3.6`, _all Poetry users who depend on your package will
have to have a cap on the Python version_. It doesn't matter if you've read the
discussion above and agree with every single line; if you depend on just a
single package that caps Python and use Poetry, you _must_ add the cap. And, if
that dependency (after reading this discussion) removes the cap, you will still
be capped. Even if they removed the cap in a patch version so therefore it does
not apply to you anymore. Personally, I believe a resolver should only force
limits on you if you pin a dependency _exactly_. Any pin that allows a single
"newer" patch version should never force you to duplicate the limits in those
unpinned dependencies in your file.

I've discussed this in Poetry, and as a result, instead of fixing the resolver,
fixing the default add, and/or fixing the default template, they added a page
describing why you should always cap your dependencies. Ironically, the reasons
on that page have been slowly disappearing, since the reasons were things like
"this is the only way to ensure your software works forever". Currently, at the
time of writing this, the section is now just basically a statement that you
should do it without much in the way of reasoning left.

I also do not like the dependency syntax in Poetry. I have one complaint with
the standard dependency syntax (no ability for one extra to depend on another,
solved in pip 21.2+), and Poetry doesn't actually solve that. Instead, it seems
to be overly complex, depends on long inline TOML tables (which are slightly
broken IMO since they arbitrarily don't support newlines or trailing commas),
and require as much or more repetition, and don't actually support exposing the
"dev" portion directly as an extra. If you have an extremely complex set of
dependencies, maybe this would be reasonable, but I've avoided mixing really
complex projects and Poetry. I have also asked for Poetry to also support PEP
621, and so far they have held back, saying their system is "better" than
supporting a standard they helped develop, because they are unhappy that no one
else liked their dependency syntax.  Also, you have to learn the standard
syntax anyway for PEP 518's `requires` field that Poetry depends on to work, so
you are always going to have to learn the PEP 440 syntaxes to use Poetry anyway.

Poetry was also very slow to support installing binary wheels Apple Silicon, or
even macOS 11; while most of the PyPA tooling supported it quickly. This means
that things like NumPy installed from source, which made Poetry basically
useless for scientific work for quite a while on macOS. I'd like to see them
prioritize patch releases if there's an entire OS affected - their own pinning
system forces users to make patch releases more often, but they haven't been doing
so themselves.

My recommendation would be to consider it if you are writing an application and
maybe for a library, but just make sure you fix the restrictive limits and
understand the limitations and quirks. The "all-in-one" structure is really
impressive, and using it is fun. I think the new plugin system will likely make
it even more popular. But using individual tools is more Pythonic, and lets you
select exactly what you like best for each situation. Flit is just as
simple/simpler than Poetry, and supports [PEP 621][] (even if rather secretly at
the moment). Setuptools is not that bad as long as you use `setup.cfg` instead
of `setup.py`, and has `setuptools_scm`, which is really nice for some
workflows. I would recommend reading either
<https://packaging.python.org/tutorials/packaging-projects> or
<https://scikit-hep.org/developer> to see what the composable, standard tools
look like.

[PEP 440]: https://www.python.org/dev/peps/pep-0440/
[PEP 621]: https://www.python.org/dev/peps/pep-0621/
