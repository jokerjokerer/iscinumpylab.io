---
title: "Writing"
date: 2017-11-10T22:08:21-05:00
---

## Git/Jupyter Books

Here is a list of GitBooks that I have either written or helped write:

| Name                      | Description |
|---------------------------|-------------|
| [Level Up Your Python][]  | Intermediate to advanced Python, for a Princeton Research Computing Workshop. |
| [Modern CMake][]          | A fantastic, up-to-date resource for CMake the way it should be. |
| [CompClass][]             | Computational Science in Python course given at the University of Cincinnati, Fall 2018. |
| [DevelopKit][]            | Developing software in LHCb's Run 3. |
| [GitBook Plugin - Term][] | A powerful terminal formatting plugin (used in other GitBooks). |
| [CLI11 Tutorial][]        | Command line parsing made beautiful. |
| [UC ROOT Tutorial][]      | Basic ROOT for HEP, conversion from old material. |
| [GooFit 2Torial][]        | Using GooFit, writing GooFit. |

[Level Up Your Python]: https://henryiii.github.io/level-up-your-python
[DevelopKit]: https://lhcb.github.io/developkit-lessons/first-development-steps/
[Modern CMake]: https://cliutils.gitlab.io/modern-cmake/
[CompClass]: https://github.com/henryiii/compclass
[GitBook Plugin - Term]: https://cliutils.gitlab.io/plugin-term
[CLI11 Tutorial]: https://cliutils.github.io/CLI11/book/
[UC ROOT Tutorial]: https://goofit.gitlab.io/root-tutorial
[GooFit 2Torial]: https://goofit.gitlab.io/Goo2Torial

## Tutorials and workshops

| Name | Description |
|-----|------|
| [HSF Modern CMake workshop](https://hsf-training.github.io/hsf-training-cmake-webpage/) | Workshop on CMake for ATLAS students at LBNL. |
| [Python CPU minicourse](https://github.com/henryiii/python-performance-minicourse) | Minicourse for high-performance CPU programming. |
| [Python GPU minicourse](https://github.com/henryiii/pygpu-minicourse) | Minicourse for GPU programming. |
| [Pandas demo](https://github.com/henryiii/pandas-notebook) | Demo of Pandas. |

## Websites

These are websites I have either created or worked on.

| Name                 | About | Platform |
|----------------------|-------|----------|
| [CLARIPHY][]         | Artificial Intelligence research to enable discoveries in particle physics. | Jekyll |
| [Science Responds][] | Information source hub for COVID-19 related research, resources, and research projects. | Jekyll |
| [IRIS-HEP][]         | Institute for sustainable software site. Includes Ruby plugins. | Jekyll |
| [Scikit-HEP][]       | Org for Python in HEP. | Jekyll |
| [ISciNumPy][]        | My blog over programming. | Hugo |
| [UC Henry][]         | My UC centric blog and site. | Hugo |
| [SSE ML LHCb][]      | Written in Hugo. | Hugo |
| [GooFit][]           | Written in Jekyll. | Jekyll |
| [MayaMuon][]         | Written in Nikola. | Nikola |
| [PHY102M][]          | Pure HTML. | HTML |
| [Angelo State HSA][] | My site has been replaced, but the logo is still my design. | HTML |
| Angelo State Society of Physics Students | No longer up. | Drupal |

[CLARIPHY]:         https://clariphy.org
[Science Responds]: https://science-responds.org
[IRIS-HEP]:         https://iris-hep.org
[Scikit-HEP]:       https://scikit-hep.org
[ISciNumPy]:        https://iscinumpy.gitlab.io
[UC Henry]:         https://hschrein.web.cern.ch/hschrein/
[SSE ML LHCb]:      https://sse-ml-lhcb.gitlab.io
[GooFit]:           https://goofit.github.io
[MayaMuon]:         http://www.hep.utexas.edu/mayamuon/aboutus.html
[PHY102M]:          https://web2.ph.utexas.edu/~phy102m/
[Angelo State HSA]: http://www.angelo.edu/org/hsa/
