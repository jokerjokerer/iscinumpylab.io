" On startup, Vim Plug: https://github.com/junegunn/vim-plug will be
" installed. Also, all initial plugs will also be installed.
" After changing the Plugs or adding this fill run :PlugInstall
" Use git config --global core.editor $(which vim) to ensure you do
" not run system VIM!
"
" Based on searches from https://vimawesome.com

" Automatic install if not yet installed
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Vim-Plug
call plug#begin('~/.vim/plugged')

" Nicer defaults
Plug 'tpope/vim-sensible'

" Nicer searching
Plug 'haya14busa/is.vim'

" Nicer status bar
Plug 'vim-airline/vim-airline'

" Code completer for VIM
" Requires brew install vim or macvim
Plug 'valloric/youcompleteme', {'do':'./install.py --clangd-completer --ts-completer'}

" Github marks in the gutter
Plug 'airblade/vim-gitgutter'

" Pretty for HTML + Jinga2
Plug 'Glench/Vim-Jinja2-Syntax'

" File browser
Plug 'scrooloose/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'

" Support EditorConfig
Plug 'editorconfig/editorconfig-vim'

" Remember position when entering a file
Plug 'farmergreg/vim-lastplace'

" Git support in vim
Plug 'tpope/vim-fugitive'

call plug#end()

" Nicer powerline
let g:airline_powerline_fonts = 1

" Nerdtree customize
map <C-n> :NERDTreeToggle<CR>

" Extra customization
set mouse=a
set spell

" Needed to use is.vim
:set hlsearch

" MacVim needs a nicer font too
set guifont=SauceCodePro\ Nerd\ Font

" Reasonable indentations
set expandtab tabstop=4 shiftwidth=4 softtabstop=4

" Putting should not copy too by default in visual mode
vnoremap p "_dP

autocmd Filetype python setlocal expandtab tabstop=4 shiftwidth=4 softtabstop=4
autocmd Filetype ruby setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2
autocmd Filetype html setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2
autocmd Filetype markdown setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2
autocmd Filetype css setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2
autocmd Filetype scss setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2
autocmd Filetype cmake setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2
